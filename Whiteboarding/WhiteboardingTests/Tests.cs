using Microsoft.VisualStudio.TestTools.UnitTesting;
using Whiteboarding;
using System;
using System.Collections.Generic;

namespace Whiteboarding.Tests
{
    [TestClass]
    public class WhiteboardingTests
    {
        Example example;

        [TestInitialize]
        public void Initialize()
        {
            example = new Example();
        }

        [TestMethod]
        public void PalindromeTestValuesTrue()
        {
            Assert.AreEqual(true, example.IsPalindrome("racecar"));
            Assert.AreEqual(true, example.IsPalindrome("10801"));
            Assert.AreEqual(true, example.IsPalindrome("madam"));
            Assert.AreEqual(true, example.IsPalindrome("MAdam"));
        }

        [TestMethod]
        public void PalindromeTestValuesFalse()
        {
            Assert.AreEqual(false, example.IsPalindrome("minivan"));
            Assert.AreEqual(false, example.IsPalindrome("A"));
            Assert.AreEqual(false, example.IsPalindrome(""));
        }

        [TestMethod]
        public void SimplePalindromeSentence()
        {
            Assert.AreEqual(true, example.IsSentencePalindrome("sator arepo tenet opera rotas"));
        }
    }

}
