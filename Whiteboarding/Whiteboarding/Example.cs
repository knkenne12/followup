﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Whiteboarding
{
    public class Example
    {

        public bool IsPalindrome(string word)
        {
            bool result = true;

            if (word != null)
            {
                string wordToLower = word.ToLower();

                if (wordToLower.Length <= 1)
                {
                    result = false;
                }

                for (int i = 0; i < (wordToLower.Length / 2); i++)
                {

                    if ((wordToLower[i]) != wordToLower[wordToLower.Length - i - 1])
                    {
                        result = false;
                    }
                }
            }

            return result;
        }

        public bool IsSentencePalindrome(string sentence)
        {
            bool result = true;

            if (sentence != null)
            {
                string spacesRemoved = sentence.Replace(" ", "");

                if (spacesRemoved.Length <= 1)
                {
                    result = false;
                }

                for (int i = 0; i < (spacesRemoved.Length / 2); i++)
                {

                    if ((spacesRemoved[i]) != spacesRemoved[spacesRemoved.Length - i - 1])
                    {
                        result = false;
                    }
                }
            }
            return result;
        }
    }
}
